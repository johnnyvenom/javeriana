This repository contains the lecture slides and supplimentary files used in my lectures for the U. Javeriana Summer 2019 course on Live Interface design. 

Contact:  
**John Sullivan**

Email: john.sullivan2@mail.mcgill.ca  
IDMIL: http://www-new.idmil.org  
CIRMMT: https://cirmmt.org  
McGill Music Technology Area: https://mt.music.mcgill.ca/research_topics


Course info: http://escueladeverano.javerianaeducacioncontinua.com/audio-vivo/

Course Dirctor: Gustavo Ramirez (ramirez.g@javeriana.edu.co)

----

[![Lecture 1 cover](https://media.johnnyvenom.com/lecture1_cover.jpg)](lecture-1)

Lecture 1

----

[![Lecture 2 cover](https://media.johnnyvenom.com/lecture2_cover.jpg)](leture-2/)

Lecture 2

----

[![Lecture 3 cover](https://media.johnnyvenom.com/lecture3_cover.jpg)](lecture-3/)

Lecture 3

----

