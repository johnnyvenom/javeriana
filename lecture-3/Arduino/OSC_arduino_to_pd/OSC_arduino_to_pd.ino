/*
 * OSC_arduino_to_Pd
 * 
 * demonstrates formatting and sending SLIP encoded
 * OSC messages over serial port that an be decoded 
 * in Pure Data
 * 
 * See accompanying OSC_arduino_to_pd.pd patch
 * 
 * connect: 
 * potentiometer to A0
 * light sensor to A1
 * button to D4
 * LED to D3
 * 
 */

#include <OSCMessage.h>

// different libraries depending on board
#ifdef BOARD_HAS_USB_SERIAL
#include <SLIPEncodedUSBSerial.h>
SLIPEncodedUSBSerial SLIPSerial( thisBoardsSerialUSB );
#else
#include <SLIPEncodedSerial.h>
 SLIPEncodedSerial SLIPSerial(Serial1);
#endif


#define KNOB A0
#define LIGHT A1
#define LED 3
#define BUTTON 4

// each sensor has a variable for the normalized sensor value
// and the value from the last message sent
float knobVal = 0;
float lastKnobVal = 0;
float lightVal = 0;
float lastLightVal = 0;
bool buttonVal = 0;
bool lastButtonVal = 0;

float threshold = 0.005; // amount of sensor change to trigger message

void setup() {
  //begin SLIPSerial just like Serial
  SLIPSerial.begin(9600);   // set this as high as you can reliably run on your platform
#if ARDUINO >= 100
  while(!Serial)
    ; //Leonardo "feature"
#endif
}

void readSensors() {
  // gets sensor values and normalizes the analog values between 0 and 1
  knobVal = analogRead(KNOB);
  knobVal = knobVal/1024;
  lightVal = analogRead(LIGHT);
  lightVal = lightVal/1024;
  buttonVal = digitalRead(BUTTON);
}

void loop(){
  // read the sensors
  readSensors();

  // *** Potentiometer ***
  float diff = knobVal - lastKnobVal; // difference between current and last sent vals
  if (abs(diff) > threshold) {
    
    //the message wants an OSC address as first argument
    OSCMessage msg("/leonardo/analog/knob");
    msg.add(knobVal); // add data as additional argument(s)
    // can add more arguments using the msg.add() method
    
    // then send the OSC message
    SLIPSerial.beginPacket();  
    msg.send(SLIPSerial); // send the bytes to the SLIP stream
    SLIPSerial.endPacket(); // mark the end of the OSC Packet
    msg.empty(); // free space occupied by message

    analogWrite(LED, byte(knobVal*255));
    lastKnobVal = knobVal; // store new last sent value
  }

  // *** Light sensor ***
  diff = lightVal - lastLightVal; // difference between current and last sent vals
  if (abs(diff) > threshold) {
    
    //the message wants an OSC address as first argument
    OSCMessage msg("/leonardo/analog/lightSensor");
    msg.add(lightVal); // add data as additional argument(s)
    // can add more arguments using the msg.add() method
    
    // then send the OSC message
    SLIPSerial.beginPacket();  
    msg.send(SLIPSerial); // send the bytes to the SLIP stream
    SLIPSerial.endPacket(); // mark the end of the OSC Packet
    msg.empty(); // free space occupied by message

    lastLightVal = lightVal; // store new last sent value
  }

    // *** Button ***
  if (buttonVal != lastButtonVal) {
    
    //the message wants an OSC address as first argument
    OSCMessage msg("/leonardo/digital/button");
    msg.add(buttonVal); // add data as additional argument(s)
    // can add more arguments using the msg.add() method
    
    // then send the OSC message
    SLIPSerial.beginPacket();  
    msg.send(SLIPSerial); // send the bytes to the SLIP stream
    SLIPSerial.endPacket(); // mark the end of the OSC Packet
    msg.empty(); // free space occupied by message

    lastButtonVal = buttonVal; // store new last sent value
  }
  
  // it is good to add a small delay in the
  // loop to avoid overloading the port.
  delay(2); 
}
