/*
 * OSC_Pd_to_arduino.ino
 * 
 * demonstrates receiving SLIP encoded
 * OSC messages over serial port from PureData
 * 
 * See accompanying OSC_pd_to_arduino.pd patch
 * 
 * connect: 
 * LED to D3
 * 
 */

#include <OSCBundle.h>
#include <OSCBoards.h>

// different libraries depending on board
#ifdef BOARD_HAS_USB_SERIAL
#include <SLIPEncodedUSBSerial.h>
SLIPEncodedUSBSerial SLIPSerial( thisBoardsSerialUSB );
#else
#include <SLIPEncodedSerial.h>
 SLIPEncodedSerial SLIPSerial(Serial1);
#endif

#define LED 3

byte ledVal = 0;

void ledControl(OSCMessage &msg) {
  if (msg.isFloat(0)) {
    ledVal = msg.getFloat(0) * 256;
    if (ledVal < 5) { ledVal = 0; };
    analogWrite(LED, ledVal);
  }
}

void setup() {
  //begin SLIPSerial just like Serial
  SLIPSerial.begin(9600);   // set this as high as you can reliably run on your platform
#if ARDUINO >= 100
  while(!Serial)
    ; //Leonardo "feature"
#endif
}

void loop(){
  OSCBundle bundleIN;
  int size;

  while(!SLIPSerial.endofPacket())
    if( (size =SLIPSerial.available()) > 0)
    {
       while(size--)
          bundleIN.fill(SLIPSerial.read());
     }
  
  if(!bundleIN.hasError())
   bundleIN.dispatch("/pd/slider", ledControl);
}
