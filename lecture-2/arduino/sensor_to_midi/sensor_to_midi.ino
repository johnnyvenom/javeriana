/* 
 * sensor_to_midi.ino
 *  
 * sketch to demonstrate 
 * reading/writing data and
 * sending in MIDI messages
 * with Arduino Leonardo.
 * 
 * connect: 
 * potentiometer to A0
 * touch sensor to D4
 * LED to D3
 * 
 */

#include "MIDIUSB.h"

#define KNOB A0
#define LED 3
#define TOUCH 4

int knobVal = 0;
int lastKnobVal = 0;
bool touchVal = 0;
bool lastTouchVal = 0;

int ccVal = 0;
int midiNote = 0;

// First parameter is the event type (0x09 = note on, 0x08 = note off).
// Second parameter is note-on/note-off, combined with the channel.
// Channel can be anything between 0-15. Typically reported to the user as 1-16.
// Third parameter is the note number (48 = middle C).
// Fourth parameter is the velocity (64 = normal, 127 = fastest).

void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
}

// First parameter is the event type (0x0B = control change).
// Second parameter is the event type, combined with the channel.
// Third parameter is the control number number (0-119).
// Fourth parameter is the control value (0-127).

void controlChange(byte channel, byte control, byte value) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}

void setup() {
  // put your setup code here, to run once:
  pinMode(TOUCH, INPUT);
  Serial.begin(9600);
  while(!Serial) {}
  Serial.print("Touch sensor: ");
  Serial.println(touchVal);
}

void readSensors() {
  knobVal = map(analogRead(KNOB), 0, 1023, 0, 127);
  touchVal = digitalRead(TOUCH);
}

void loop() {
  // read the sensors
  readSensors();

  if (knobVal != lastKnobVal) { //knob send CC 10 message
    lastKnobVal = knobVal;
    controlChange(0, 10, knobVal);
    MidiUSB.flush();
    analogWrite(LED, knobVal*2);
    Serial.print("CC 10: ");
    Serial.println(knobVal);
  }
  
  // touch sensor triggers random note sent on MIDI channel 1. 
  // only print if it changes value.
  if (touchVal != lastTouchVal) {
    lastTouchVal = touchVal;
    if (touchVal == 1) {
      midiNote = random(24, 60);
      noteOn(0, midiNote, 64);
      MidiUSB.flush();
      Serial.print("Note on: ");
      Serial.println(midiNote);
    } else {
      noteOff(0, midiNote, 64);
      MidiUSB.flush();
      Serial.print("Note off: ");
      Serial.println(midiNote);
      analogWrite(LED, 0);
    }
  }

  delay(2);
}
